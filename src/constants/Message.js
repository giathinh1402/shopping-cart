export const MSG_ADD_TO_CART_SUCCESS = 'Thêm sản phẩm thành công! ';
export const MSG_UPDATE_TO_CART_SUCCESS = 'Cập nhật giỏ hàng thành công!';
export const MSG_DELETE_PRODUCT_IN_CART_SUCCESS = 'Xoá sản phẩm khỏi giỏ hàng thành công';
export const MSG_CART_EMPTY = 'Chưa có sẩn phẩm trong giỏ hàng!';
export const MSG_WELCOME = 'Chào mừng đến với shop bán hàng online!';